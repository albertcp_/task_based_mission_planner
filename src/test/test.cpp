#include <gtest/gtest.h>
#include <Mission.h>
#include <Interpreter.h>
#include <Definitions.h>
#include <limits.h>
#include <unistd.h>

#define FILE_NOT_FOUND_MSG COLOR_RED \
  "[ERROR] Mission specification file has not been found. Parsing cannot continue\n" \
  COLOR_RESET

#define UNBALANCED_TAGS_MSG_0 COLOR_RED \
  "[ERROR] Start-end tags mismatch: line 21, column 16\n" \
  COLOR_RESET

#define UNBALANCED_TAGS_MSG_1 COLOR_RED \
  "[ERROR] Start-end tags mismatch: line 22, column 10\n" \
  COLOR_RESET

#define MISSION_TAG_NOT_FOUND_MSG COLOR_RED \
  "[ERROR] Expected tag '<mission macro_task=\"macro_task_name\">' not found\n" \
  COLOR_RESET

#define SEVERAL_SYNTAX_ERRORS_MSG_0 COLOR_RED \
  "[ERROR] Expected attribute 'name' not found\n" \
  "[ERROR] Unexpected tag 'actionn' within task 'Stabilize'\n" \
  "[ERROR] Expected attribute 'name' not found\n" \
  "[ERROR] Not enough arguments for action \"GO_TO_POINT\". Argument \"coordinates\" is needed.\n" \
  "[ERROR] Task names cannot be repeated. Children of task 'Test7' have repeated names\n" \
  COLOR_RESET

#define SEVERAL_SYNTAX_ERRORS_MSG_1 COLOR_RED \
  "[ERROR] Attribute 'name' not found in event tag\n" \
  "[ERROR] Incorrect declaration for condition. " \
    "Expected: <condition parameter=\"cond_parm\" comparison=\"cond_comp\" value=\"cond_val\" />\n" \
  "[ERROR] Expected 'call' tag within event ''\n" \
  "[ERROR] Unexpected tag name 'eventx'. Expected: 'event'\n" \
  "[ERROR] Expected 'condition' tag within event ''\n" \
  "[ERROR] Expected 'call' tag within event ''\n" \
  "[ERROR] Expected 'termination' tag within event ''\n" \
  "[ERROR] Event names cannot be repeated.\n" \
  COLOR_RESET

#define MACRO_TASK_NOT_FOUND_MSG COLOR_RED \
  "[ERROR] Macro task 'Test 10' not found\n" \
  COLOR_RESET

std::string stack_path, test_path;

TEST(CompilerTest, MissionWithoutErrors0)
{
  std::string mission_path("test_0.xml");
  Interpreter interpreter;
  Mission mission = interpreter.generateMission(stack_path + test_path + mission_path);

  std::cout << interpreter.getErrors();
  EXPECT_TRUE(interpreter.getErrors().empty());
  VariableHandler::reset();
}

TEST(CompilerTest, MissionWithoutErrors1)
{
  std::string mission_path("test_1.xml");
  Interpreter interpreter;
  Mission mission = interpreter.generateMission(stack_path + test_path + mission_path);

  std::cout << interpreter.getErrors();
  EXPECT_TRUE(interpreter.getErrors().empty());
  VariableHandler::reset();
}

TEST(CompilerTest, MissionWithoutErrors2)
{
  std::string mission_path("test_2.xml");
  Interpreter interpreter;
  Mission mission = interpreter.generateMission(stack_path + test_path + mission_path);

  std::cout << interpreter.getErrors();
  EXPECT_TRUE(interpreter.getErrors().empty());
  VariableHandler::reset();
}

TEST(CompilerTest, MissionWithoutErrors3)
{
  std::string mission_path("test_3.xml");
  Interpreter interpreter;
  Mission mission = interpreter.generateMission(stack_path + test_path + mission_path);

  std::cout << interpreter.getErrors();
  EXPECT_TRUE(interpreter.getErrors().empty());
  VariableHandler::reset();
}

TEST(CompilerTest, FileNotFoundTest)
{
  std::string mission_path("phantom_test.xml");
  Interpreter interpreter;
  Mission mission = interpreter.generateMission(stack_path + test_path + mission_path);

  EXPECT_TRUE(!interpreter.getErrors().empty());
  EXPECT_EQ(interpreter.getErrors(), FILE_NOT_FOUND_MSG);
  VariableHandler::reset();
}

TEST(CompilerTest, UnbalancedTagsTest0)
{
  std::string mission_path ("test_4.xml");
  Interpreter interpreter;
  Mission mission = interpreter.generateMission(stack_path + test_path + mission_path);

  EXPECT_TRUE(!interpreter.getErrors().empty());
  EXPECT_EQ(interpreter.getErrors(), UNBALANCED_TAGS_MSG_0);
  VariableHandler::reset();
}

TEST(CompilerTest, UnbalancedTagsTest1)
{
  std::string mission_path ("test_5.xml");
  Interpreter interpreter;
  Mission mission = interpreter.generateMission(stack_path + test_path + mission_path);

  EXPECT_TRUE(!interpreter.getErrors().empty());
  EXPECT_EQ(interpreter.getErrors(), UNBALANCED_TAGS_MSG_1);
  VariableHandler::reset();
}

TEST(CompilerTest, SyntaxErrorsTest0)
{
  std::string mission_path ("test_6.xml");
  Interpreter interpreter;
  Mission mission = interpreter.generateMission(stack_path + test_path + mission_path);

  EXPECT_TRUE(!interpreter.getErrors().empty());
  EXPECT_EQ(interpreter.getErrors(), MISSION_TAG_NOT_FOUND_MSG);
  VariableHandler::reset();
}

TEST(CompilerTest, SyntaxErrorsTest1)
{
  std::string mission_path ("test_7.xml");
  Interpreter interpreter;
  Mission mission = interpreter.generateMission(stack_path + test_path + mission_path);

  EXPECT_TRUE(!interpreter.getErrors().empty());
  EXPECT_EQ(interpreter.getErrors(), SEVERAL_SYNTAX_ERRORS_MSG_0);
  VariableHandler::reset();
}

TEST(CompilerTest, SyntaxErrorsTest2)
{
  std::string mission_path ("test_8.xml");
  Interpreter interpreter;
  Mission mission = interpreter.generateMission(stack_path + test_path + mission_path);

  EXPECT_TRUE(!interpreter.getErrors().empty());
  EXPECT_EQ(interpreter.getErrors(), SEVERAL_SYNTAX_ERRORS_MSG_1);
  VariableHandler::reset();
}
/*
TEST(CompilerTest, SyntaxErrorsTest3)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_9.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  
  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 2);
  EXPECT_EQ(error_msgs.at(0), UNEXPECTED_TAG_EVENT_MSG);
  EXPECT_EQ(error_msgs.at(1), CONDITION_SYNTAX_ERROR_MSG_0);
}

TEST(CompilerTest, SyntaxErrorsTest4)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_11.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  
  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 3);
  EXPECT_EQ(error_msgs.at(0), ACTION_ARGUMENTS_MSG_5);
  EXPECT_EQ(error_msgs.at(1), NOT_DECLARED_VARIABLE_MSG_1 );
  EXPECT_EQ(error_msgs.at(2), NOT_DECLARED_VARIABLE_MSG_1 );

}

TEST(CompilerTest, SyntaxErrorsTest5)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_12.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  
  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 1);
  EXPECT_EQ(error_msgs.at(0), ACTION_ARGUMENTS_MSG_6);
}

TEST(CompilerTest, SyntaxErrorsTest6)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_13.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  
  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 1);
  EXPECT_EQ(error_msgs.at(0), CONDITION_SYNTAX_ERROR_MSG_1);
}

TEST(CompilerTest, SyntaxErrorsTest8)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_15.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  
  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 1);
  EXPECT_EQ(error_msgs.at(0), TERMINATION_SYNTAX_ERROR_MSG_0);
}
*/
TEST(CompilerTest, SemanticErrorsTest0)
{
  std::string mission_path ("test_10.xml");
  Interpreter interpreter;
  Mission mission = interpreter.generateMission(stack_path + test_path + mission_path);

  EXPECT_TRUE(!interpreter.getErrors().empty());
  EXPECT_EQ(interpreter.getErrors(), MACRO_TASK_NOT_FOUND_MSG);
  VariableHandler::reset();
}
/*
TEST(CompilerTest, SemanticErrorsTest1)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_3.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  EXPECT_TRUE(error_msgs.empty());
}

TEST(CompilerTest, SemanticErrorsTest2)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_19.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  EXPECT_TRUE(error_msgs.empty());
}

TEST(CompilerTest, SemanticErrorsTest3)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_20.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  EXPECT_FALSE(error_msgs.empty());
  ASSERT_EQ(error_msgs.size(), 1);
  EXPECT_EQ(error_msgs.at(0), ACTION_ARGUMENTS_MSG_7);
}

TEST(CompilerTest, SemanticErrorsTest4)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_21.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  EXPECT_FALSE(error_msgs.empty());
  ASSERT_EQ(error_msgs.size(), 1);
  EXPECT_EQ(error_msgs.at(0), ACTION_ARGUMENTS_MSG_8);
}

TEST(MissionTest, TreeTraversalTest1)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_1.xml");
  Parser parser(stack_path + test_path + mission_path);
  std::vector<Event> events = parser.generateEventVector();
  Parameter_Handler parameter_handler;
  Mission mission(parser.generateTaskTree(), parameter_handler);
  std::vector<argument<std::string,std::vector<double>>>  arguments;
  Task task;

  
  ASSERT_TRUE(!mission.missionCompleted());
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Taking off");
  EXPECT_EQ(task.getAction()->getReadableName(), "TAKE_OFF");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Memorize first point");
  EXPECT_EQ(task.getAction()->getReadableName(), "MEMORIZE_POINT");
  // It is mandatory to check if the arguments memorized as labels are stored correctly.
  arguments = task.getAction()->getArguments();
  EXPECT_EQ(arguments.size(), 1);
  EXPECT_EQ(arguments.at(0).value.size(), 1);
  EXPECT_EQ(arguments.at(0).value.at(0), 0);
  task = mission.nextTask();
  // Value should be 0 because the memorized point will be the first one in being stored.
  // If another point is memorized, then its value in the previous vector should be 1.
  EXPECT_EQ(task.getDescription(), "Memorize first point again");
  EXPECT_EQ(task.getAction()->getReadableName(), "MEMORIZE_POINT");
  arguments = task.getAction()->getArguments();
  EXPECT_EQ(arguments.size(), 1);
  EXPECT_EQ(arguments.at(0).value.size(), 1);
  EXPECT_EQ(arguments.at(0).value.at(0), 1);
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Moving to A1");
  EXPECT_EQ(task.getAction()->getReadableName(), "GO_TO_POINT");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Frontflip");
  EXPECT_EQ(task.getAction()->getReadableName(), "FLIP");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Landing");
  EXPECT_EQ(task.getAction()->getReadableName(), "LAND");
  ASSERT_TRUE(mission.missionCompleted());
}

TEST(MissionTest, TreeTraversalTest2)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_2.xml");
  Parser parser(stack_path + test_path + mission_path);
  std::vector<Event> events = parser.generateEventVector();
  Parameter_Handler parameter_handler;
  Mission mission(parser.generateTaskTree(), parameter_handler);
  std::vector<argument<std::string,std::vector<double>>>  arguments;
  Task task;
  
  ASSERT_TRUE(!mission.missionCompleted());
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Taking off");
  EXPECT_EQ(task.getAction()->getReadableName(), "TAKE_OFF");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  //Checking task repetitions
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Moving to A1");
  EXPECT_EQ(task.getAction()->getReadableName(), "MOVE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Frontflip");
  EXPECT_EQ(task.getAction()->getReadableName(), "FLIP");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  //Checking task repetitions
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Moving to A1");
  EXPECT_EQ(task.getAction()->getReadableName(), "MOVE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Frontflip");
  EXPECT_EQ(task.getAction()->getReadableName(), "FLIP");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  //Finishing the mission
  EXPECT_EQ(task.getDescription(), "Landing");
  EXPECT_EQ(task.getAction()->getReadableName(), "LAND");
  ASSERT_TRUE(mission.missionCompleted());
}

TEST(MissionTest, TreeTraversalTest3)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_16.xml");
  Parser parser(stack_path + test_path + mission_path);
  std::vector<Event> events = parser.generateEventVector();
  Parameter_Handler parameter_handler;
  Mission mission(parser.generateTaskTree(), parameter_handler);
  Task task;
  
  ASSERT_TRUE(!mission.missionCompleted());
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Take Off");
  EXPECT_EQ(task.getAction()->getReadableName(), "TAKE_OFF");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "hover");
  EXPECT_EQ(task.getAction()->getReadableName(), "WAIT");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "remember coordinates");
  EXPECT_EQ(task.getAction()->getReadableName(), "MEMORIZE_POINT");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Move 3");
  EXPECT_EQ(task.getAction()->getReadableName(), "GO_TO_POINT");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilize");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Land");
  EXPECT_EQ(task.getAction()->getReadableName(), "LAND");
  ASSERT_TRUE(mission.missionCompleted());
}

TEST(MissionTest, TreeTraversalTest4)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_17.xml");
  Parser parser(stack_path + test_path + mission_path);
  std::vector<Event> events = parser.generateEventVector();
  Parameter_Handler parameter_handler;
  Mission mission(parser.generateTaskTree(), parameter_handler);
  Task task;
  
  ASSERT_TRUE(!mission.missionCompleted());
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Take Off");
  EXPECT_EQ(task.getAction()->getReadableName(), "TAKE_OFF");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "hover");
  EXPECT_EQ(task.getAction()->getReadableName(), "WAIT");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilize");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Land");
  EXPECT_EQ(task.getAction()->getReadableName(), "LAND");
  ASSERT_TRUE(mission.missionCompleted());
}

TEST(MissionTest, TreeTraversalTest5)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_19.xml");
  Parser parser(stack_path + test_path + mission_path);
  std::vector<Event> events = parser.generateEventVector();
  Parameter_Handler parameter_handler;
  Mission mission(parser.generateTaskTree(), parameter_handler);
  std::vector<argument<std::string,std::vector<double>>>  arguments;
  Task task;

  ASSERT_TRUE(!mission.missionCompleted());
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Take off");
  EXPECT_EQ(task.getAction()->getReadableName(), "TAKE_OFF");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilize at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Activate visual servoing");
  EXPECT_EQ(task.getAction()->getReadableName(), "FOLLOW_IMAGE");
  arguments = task.getAction()->getArguments();
  EXPECT_EQ(arguments.size(), 1);
  EXPECT_EQ(arguments.at(0).name, "duration");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Land");
  EXPECT_EQ(task.getAction()->getReadableName(), "LAND");
  task = mission.nextTask();
  ASSERT_TRUE(mission.missionCompleted());
}
*/
int main (int argc, char **argv)
{
  stack_path = getenv("AEROSTACK_STACK");
  test_path = "/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/";

  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
