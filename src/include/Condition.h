/*!*****************************************************************************
 *  \file    Condition.h
 *  \brief   Definition of all the classes used in the file
 *           Condition.cpp .
 *
 *  \author  Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef CONDITION_H
#define CONDITION_H
#include "Definitions.h"
#include "pugixml.hpp"
#include "ParameterHandler.h"
#include "VariableHandler.h"
#include <iostream>
#include <cstdlib>

/*!***************************************************************************
 *  \class Condition
 *  \brief This class represents a definition of what a condition is.
 *****************************************************************************/
class Condition{
private:
  bool condition_evaluation;

  std::string belief;

public:
  //Constructor & Destructor
  Condition();
  ~Condition();

public:
  /*!************************************************************************
   *  \brief  This method returns the belief expression of the condition.
   *  \return A string with the belief expression.
   *************************************************************************/
  std::string getBelief();

  // Returns string with errors found or empty string if no errors
  std::string check(pugi::xml_node condition_node);

};
#endif
