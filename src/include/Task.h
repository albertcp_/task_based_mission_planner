/*!*****************************************************************************
 *  \file    Task.h
 *  \brief   Definition of all the classes used in the file
 *           Task.cpp .
 *
 *  \author  Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef TASK_H
#define TASK_H

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <cmath>
#include <deque>
#include "pugixml.hpp"
#include "Condition.h"
#include "Definitions.h"

/*!***************************************************************************
 *  \class Task
 *  \brief This class represents an abstraction of a Task.
 *****************************************************************************/
class Task
{
private:
  std::string name;

  std::deque<Task> tasks;
  std::deque<Command> commands;
  std::deque<Command> end_commands;

  std::deque<Task> original_tasks;
  std::deque<Command> original_commands;

  std::vector<Condition> conditions;
  int times_to_repeat;

public:
  std::string getName();

  Command nextCommand();

  std::vector<Condition> getConditions();
  /*!************************************************************************
   *  \brief  This method returns the absolute name of the task within the
   *          mission's task tree.
   *  \return A string with the absolute name.
   *************************************************************************/
  int getTimesToRepeat();

  void reset();

  // Returns string with errors found or empty string if no errors
  std::string check(pugi::xml_node task_node);

public:
  Task();  // Class constructor
  ~Task();   // Class destructor
};
#endif
