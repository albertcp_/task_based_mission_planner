/*!*****************************************************************************
 *  \file    VariableHandler.h
 *  \brief   Definition of all the classes used in the file
 *           VariableHandler.cpp
 *
 *  \author  Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef VARIABLE_HANDLER
#define VARIABLE_HANDLER

#include <map>
#include <string>
#include <vector>

class VariableHandler
{
private:
  VariableHandler();
  ~VariableHandler();

  static std::map<std::string, std::string> table_of_variables;
  static std::vector<std::string> table_of_macro_task_names;

public:
  static void setVariable(std::string key, std::string value);
  static std::string getVariable(std::string key);
  static bool labelExists(std::string key);

  static void setMacroTaskName(std::string name);
  static bool macroTaskExists(std::string key);

  static void reset();
 /*!************************************************************************
   *  \brief  This function transforms a string in point format into a vector
   *          of 3 real coordinates.
   *  \param  s The string that is going to be transformed.
   *  \return A vector of lenght 3.
   *************************************************************************/
  static std::vector<double> pointToVector(std::string s);

private:

};

#endif
