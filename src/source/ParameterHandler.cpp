#include "ParameterHandler.h"
using namespace std;

ParameterHandler::ParameterHandler()
{
}

ParameterHandler::~ParameterHandler()
{
}

void ParameterHandler::updateTableOfParameters(std::pair<std::string, std::string> entry)
{
  bool found = false;
  for(uint i = 0; i < table_of_parameters.size() && !found; i++)
  {
    if(table_of_parameters.at(i).first == entry.first)
    {
      table_of_parameters.at(i).second = entry.second;
      found = true;
    }

  }
  if(!found)
    table_of_parameters.push_back(entry);
}

void ParameterHandler::removeFromTableOfParameters(std::string key)
{
  for(int i=0; i<table_of_parameters.size(); i++)
  {
    if(table_of_parameters[i].first == key)
    {
      table_of_parameters.erase(table_of_parameters.begin() + i);
      break;
    }
  }
}

std::string ParameterHandler::fromIntVectorToString(std::vector<int> vec)
{
  //The vector is saved according following syntaxis: [id0, id1,...]
  std::string value="[";
  //Generate value.
  for (uint i = 0; i<vec.size(); i++)
  {
    if(i == vec.size()-1)
      value = value + fromNumberToString(vec.at(i));
    else
      value = value + fromNumberToString(vec.at(i))+", ";
  }
  value = value +"]";
  return value;
}

std::string ParameterHandler::fromNumberToString(double n)
{
  //TO DO: Need to check if the given argument is a number.
  return to_string(n);
}

bool ParameterHandler::existsInTableOfParameters(std::string par_name)
{
  bool found = false;
  for(uint i = 0; i<table_of_parameters.size() && !found; i++)
    found = (table_of_parameters.at(i).first == par_name);
  return found;
}
std::pair<std::string,std::string> ParameterHandler::findInTableOfParameters(std::string par_name)
{
  //It is necessary to check if the element exists in the table of parameters.
  bool found = false;
  std::pair<std::string,std::string> element;
  for(uint i=0; i< table_of_parameters.size(); i++)
  {
    found = (table_of_parameters.at(i).first == par_name);
    if(found)
      element = std::make_pair(par_name, table_of_parameters.at(i).second);
  }
  return element;
}

void ParameterHandler::reset()
{
  for(std::pair<std::string, std::string> entry: table_of_parameters)
  {
    entry.second = "";
  }
}


// Initialization of static members
std::vector<std::pair<std::string,std::string> > ParameterHandler::table_of_parameters;
