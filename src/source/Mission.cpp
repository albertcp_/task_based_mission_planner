#include <stdio.h>
#include <iostream>
#include "Mission.h"

Mission::Mission()
{
  recurrent_behaviors_before_event.push_back({});
  repeat_command_before_event = false;
  command_before_event = {.type = CmdType::none};
}
Mission::~Mission()
{
}

std::string Mission::check(pugi::xml_node mission_node)
{
  std::string errors;

  if(!mission_node.first_attribute() ||
     std::string(mission_node.first_attribute().name()) != "root_task")
  {
     errors +=
      "Expected attribute 'root_task' not found in tag '<mission>'\n";
  }
  else if(mission_node.first_attribute().next_attribute())
  {
    errors +=
      "Unexpected attribute '" +
      std::string(mission_node.first_attribute().next_attribute().name()) +
      "' found in tag '<mission>'\n";
  }
  else
  {
    main_root_task_name = std::string(mission_node.first_attribute().value());

    for(auto n: mission_node.children())
    {
      if(std::string(n.name())=="task_tree")
      {

        if(!n.first_child())
        {
          errors += "'<task_tree>' tag must contain 1 '<task>' tag\n";
        }

        else if(!n.first_child().first_attribute() ||
                std::string(n.first_child().first_attribute().name()) != "name")
        {
          errors += "Tag '<task>' must have a 'name' attribute\n";
        }

        else if(VariableHandler::macroTaskExists(n.first_child().first_attribute().value()))
        {
          errors +=
            "Root task '" + std::string(n.first_child().first_attribute().value()) +"' has been declared. "
            "Please choose another name for this root task\n";
        }
        VariableHandler::setMacroTaskName(n.first_child().first_attribute().value());
        Task task;
        errors += task.check(n.first_child());
        root_tasks.push_back(task);
      }
      else if(std::string(n.name()) == "event_handling")
      {
        errors += event_handler.check(n);
      }
      else
      {
        errors +=
          "Unexpected tag '" +
          std::string(n.name()) +
          "'\n";
      }
    }

    if(!setMainTaskTree())
    {
      errors +=
        "Root task '" + main_root_task_name + "' not found\n";
    }
  }

  return errors;
}

bool Mission::setMainTaskTree()
{
  for(Task& task: root_tasks)
  {
    if(task.getName() == main_root_task_name)
    {
      root_task_in_execution = &task;
      return true;
    }
  }
  return false;
}

bool Mission::setTaskTree(std::string root_task_name)
{
  task_before_event.push_back(root_task_in_execution);
  unfinished_commands.push_back(last_command);
  for(Behavior bhv: recurrent_behaviors_before_event.back())
  {
    urgent_commands.push_back({.type=CmdType::inhibitBehavior, .behavior=bhv});
  }
  recurrent_behaviors_before_event.push_back({});
  for(Task& task: root_tasks)
  {
    if(task.getName() == root_task_name)
    {
      task.reset();
      root_task_in_execution = &task;
      return true;
    }
  }
  return false;
}

void Mission::returnFromEvent()
{
  root_task_in_execution = task_before_event.back();
  task_before_event.pop_back();

  command_before_event = unfinished_commands.back();
  unfinished_commands.pop_back();

  recurrent_behaviors_before_event.pop_back();
  for(Behavior bhv: recurrent_behaviors_before_event.back())
  {
    urgent_commands.push_back({.type=CmdType::activateBehavior, .behavior=bhv});
  }
}

std::string Mission::getName()
{
  return main_root_task_name;
}

Command Mission::nextCommand()
{
  if(!urgent_commands.empty())
  {
    Command cmd = urgent_commands.back();
    urgent_commands.pop_back();
    return cmd;
  }

  if(repeat_command_before_event && command_before_event.type != CmdType::none)
  {
    Command cmd = command_before_event;
    repeat_command_before_event = false;
    last_command = cmd;
    return cmd;
  }

  if(!root_task_in_execution)
    return {.type = CmdType::none};

  Command cmd = root_task_in_execution->nextCommand();
  if(cmd.type == CmdType::activateBehavior && cmd.behavior.isRecurrent())
  {
    recurrent_behaviors_before_event.back().push_back(cmd.behavior);
  }
  else if(cmd.type == CmdType::inhibitBehavior && cmd.behavior.isRecurrent())
  {
    recurrent_behaviors_before_event.back().pop_back();
  }

  last_command = cmd;
  return cmd;
}

Mission& Mission::operator=(const Mission& m)
{
  root_tasks = m.root_tasks;
  main_root_task_name = m.main_root_task_name;
  setMainTaskTree();
  event_handler = m.event_handler;
  task_before_event = m.task_before_event; // This should be empty
  recurrent_behaviors_before_event = m.recurrent_behaviors_before_event;

  return *this;
}

Mission::Mission(const Mission& m)
{
  root_tasks = m.root_tasks;
  main_root_task_name = m.main_root_task_name;
  setMainTaskTree();
  event_handler = m.event_handler;
  task_before_event = m.task_before_event; // This should be empty
  recurrent_behaviors_before_event = m.recurrent_behaviors_before_event;
}

std::vector<Event> Mission::getEvents()
{
  return event_handler.getEvents();
}

std::string Mission::getCurrentRootTaskName()
{
  return root_task_in_execution->getName();
}

Task *Mission::getCurrentRootTask()
{
  return root_task_in_execution;
}

void Mission::reset()
{
  for(Task& task: root_tasks)
  {
    task.reset();
  }

  repeat_command_before_event = false;
  command_before_event = {.type = CmdType::none};
  unfinished_commands.clear();
}

bool Mission::ended()
{
  return task_before_event.empty();
}

void Mission::repeatCommandBeforeEvent()
{
  repeat_command_before_event = true;
}
