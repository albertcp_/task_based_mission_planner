#include <stdio.h>
#include <iostream>
#include "Task.h"

Task::Task()
{
}

Task::~Task()
{
}


std::vector<Condition> Task::getConditions()
{
  return conditions;
}

int Task::getTimesToRepeat()
{
  return times_to_repeat;
}

std::string Task::check(pugi::xml_node task_node)
{
  std::string errors = "";
  if(std::string(task_node.name()) != "task")
  {
    errors +=
      "Unexpected tag name '" + std::string(task_node.name()) +
      "'. Expected: 'task'\n";
  }

  if(!task_node.first_attribute() || std::string(task_node.first_attribute().name()) != "name")
  {
    errors +=
      "Expected attribute 'name' not found\n";
  }
  else if(task_node.first_attribute().next_attribute())
  {
    errors +=
      "Unexpected attribute '" +
      std::string(task_node.first_attribute().next_attribute().name()) +
      "'\n";
  }
  else
  {
    name = std::string(task_node.first_attribute().value());
  }

  pugi::xml_node child = task_node.first_child();
  while(child && std::string(child.name()) == "condition")
  {
    Condition condition;
    errors += condition.check(child);
    conditions.push_back(condition);

    child = child.next_sibling();
  }

  if(child && std::string(child.name()) == "repeat")
  {
    if(!child.first_attribute() && std::string(child.first_attribute().name()) != "times")
    {
      errors +=
        "Expected attribute 'times' not found in 'repeat' statement "
        "within task '" + name + "'\n";
    }
    else if(std::stoi(child.first_attribute().value()) <= 0 ||
            std::stoi(child.first_attribute().value()) !=
            std::floor(std::stod(child.first_attribute().value())))
    {
      errors +=
        "Value for 'times' attribute in 'repeat' statement "
        "within task '" + name + "' "
        "is expected to be a non-negative integer\n";
    }
    else if(child.first_attribute().next_attribute())
    {
      errors +=
        "Unexpected attribute '" +
        std::string(child.first_attribute().next_attribute().name()) +
        "' in 'repeat' statement within task '" + name + "'\n";
    }
    else
    {
      times_to_repeat = std::stoi(child.first_attribute().value());
    }

    child = child.next_sibling();
  }

  while(child && (std::string(child.name()) == "behavior" ||
        std::string(child.name()) == "recurrent_behavior"))
  {
    Behavior bhv;
    errors += bhv.check(child);
    commands.push_back({.type=CmdType::activateBehavior, .behavior=bhv});

    if(bhv.isRecurrent()) {
      end_commands.push_front({
        .type=CmdType::inhibitBehavior,
        .behavior=bhv
      });
    }

    child = child.next_sibling();
  }

  while(child && (std::string(child.name()) == "consult"))
  {
    if(!child.first_attribute() && std::string(child.first_attribute().name()) != "belief")
    {
      errors +=
        "<consult> tag must contain a 'belief' argument as first argument\n";
    }

    else
    {
      Command cmd;
      cmd.type = CmdType::consultBelief;
      cmd.belief = std::string(child.first_attribute().value());
      commands.push_back(cmd);
    }

    child = child.next_sibling();
  }

  while(child && (std::string(child.name()) == "add"))
  {
    if(!child.first_attribute() || std::string(child.first_attribute().name()) != "belief")
    {
      errors +=
        "<add> tag must contain a 'belief' argument as first argument\n";
    }


    else
    {
      bool multivalued = false;
      if(child.first_attribute().next_attribute() &&
      std::string(child.first_attribute().next_attribute().name()) == "multivalued")
      {
        multivalued = child.first_attribute().next_attribute().as_bool();
      }
      Command cmd;
      cmd.type=CmdType::addBelief;
      cmd.belief=std::string(child.first_attribute().value());
      cmd.belief_multivalued=multivalued;
      commands.push_back(cmd);
    }

    child = child.next_sibling();
  }

  while(child && (std::string(child.name()) == "remove"))
  {
    if(!child.first_attribute() && std::string(child.first_attribute().name()) != "belief")
    {
      errors +=
        "<remove> tag must contain a 'belief' argument as first argument\n";
    }

    else
    {
      Command cmd;
      cmd.type=CmdType::removeBelief;
      cmd.belief=std::string(child.first_attribute().value());
      commands.push_back(cmd);
    }

    child = child.next_sibling();
  }

  while(child && std::string(child.name()) == "task")
  {
    Task task;
    errors += task.check(child);
    tasks.push_back(task);

    child = child.next_sibling();
  }

  if(child)
  {
    errors +=
      "Unexpected tag '" + std::string(child.name()) +
      "' in task '" + name + "'\n";
  }

  // Save a copy of commands and tasks
  original_commands = commands;
  original_tasks = tasks;

  return errors;
}

void Task::reset()
{
  tasks = original_tasks;
  commands = original_commands;
}

Command Task::nextCommand()
{
  // if(!conditionsPass())
  //   return {.type=CmdType::none};

  if(!commands.empty())
  {
    Command cmd = commands[0];
    commands.pop_front();
    return cmd;
  }

  while(!tasks.empty())
  {
    Command cmd = tasks[0].nextCommand();
    if(cmd.type == CmdType::none)
    {
      tasks.pop_front();
    }
    else
    {
      return cmd;
    }
  }

  if(!end_commands.empty())
  {
    Command cmd = end_commands[0];
    end_commands.pop_front();
    return cmd;
  }

  return {.type=CmdType::none};
}

std::string Task::getName()
{
  return name;
}
