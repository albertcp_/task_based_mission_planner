#include "Interpreter.h"

Interpreter::Interpreter()
{
}
Interpreter::~Interpreter()
{
}

Mission Interpreter::generateMission(std::string mission_specification_file_path)
{
  Mission mission;

  // Cleanup from existing missions
  ParameterHandler::reset();
  VariableHandler::reset();

  pugi::xml_document mission_specification_file;

  std::string errors;
  //Reading and loading file.
  std::ifstream nameFile(mission_specification_file_path.c_str());
  pugi::xml_parse_result result = mission_specification_file.load(nameFile);

  //Parsing errors
  errors += checkParsingErrors(result, mission_specification_file_path);
  if(errors != "")
  {
    errors_found = formatErrors(errors);
    return mission;
  }

  pugi::xml_node mission_specification_node = mission_specification_file.first_child();

  if(!mission_specification_node ||
     std::string(mission_specification_node.name()) != "mission")
  {
    errors +=
      "Expected tag '<mission root_task=\"root_task_name\">' not found\n";
  }
  else
  {
    errors += mission.check(mission_specification_node);
  }

  if(errors != "")
    errors_found = formatErrors(errors);

  return mission;
}

bool Interpreter::missionStatus(std::string& status)
{
  if(!errors_found.empty())
    status = errors_found;
  else
  {
    status = COLOR_GREEN
      "File loaded without errors! Waiting to start ..."
      COLOR_RESET "\n";
  }

  return errors_found.empty();
}

std::string Interpreter::checkParsingErrors(pugi::xml_parse_result result, std::string missionConfigFile)
{
  std::string errors;
  if(!fileExists(missionConfigFile))
    errors += "Mission specification file has not been found. Parsing cannot continue\n";
  else if (!result)
  {
    offset_data_t offset_data;
    std::string message = result.description();
    //Obtaining line and column.
    buildOffsetData(offset_data, missionConfigFile);
    std::pair<int, int> num = getLocation(offset_data, result.offset);
    errors +=
      message + ": line " + std::to_string(num.first) +
      ", column " + std::to_string(num.second) + "\n";
  }
  return errors;
}

std::pair<int, int> Interpreter::getLocation(const offset_data_t& data, ptrdiff_t offset)
{
  offset_data_t::const_iterator it = std::lower_bound(data.begin(), data.end(), offset);
  size_t index = it - data.begin();

  return std::make_pair(1 + index, index == 0 ? offset + 1 : offset - data[index - 1]);
}

void Interpreter::buildOffsetData(offset_data_t& result, std::string file)
{
  FILE* f = fopen(file.c_str(), "rb");
  ptrdiff_t offset = 0;

  char buffer[1024];
  size_t size;

  while((size = fread(buffer, 1, sizeof(buffer), f)) > 0)
  {
    for(size_t i = 0; i < size; ++i)
      if(buffer[i] == '\n')
        result.push_back(offset + i);
    offset += size;
  }
  fclose(f);
}

inline bool Interpreter::fileExists(const std::string& name)
{
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}

std::string Interpreter::formatErrors(std::string message)
{
  std::string format = "[ERROR] ";
  std::string formatted_errors = format + message;

  uint errors_inserted = 1;
  for(uint i=0; i<message.size()-1; i++) // Don't add at the end (size-1)
  {
    char c = message.at(i);
    if(c == '\n')
    {
      uint pos = i + (errors_inserted * format.size()) + 1;
      formatted_errors.insert(pos, format);
      errors_inserted++;
    }
  }

  return COLOR_RED + formatted_errors + COLOR_RESET;
}

std::string Interpreter::getErrors()
{
  return errors_found;
}
